---
layout: page
title: Research
permalink: /maths/
---

* Some numerical aspects of MFGs. J. Frédéric Bonnans, Pierre Lavigne and Laurent Pfeiffer. In preparation.

* Discrete-time mean field games with risk averse-agents. J. Frédéric Bonnans, Pierre Lavigne and Laurent Pfeiffer (2020). [Preprint](https://arxiv.org/abs/2005.02232).
