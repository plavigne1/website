---
layout: page
title: Events
---

* Autumn 2021: [Congrès des jeunes chercheurs en mathématiques appliqués](https://cjc-ma2021.github.io). Incoming event in applied mathematics in Paris, organized by young researcher and supported by [SMAI](http://smai.emath.fr).
